/**
 * BibSonomy-Common - Common things (e.g., exceptions, enums, utils, etc.)
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.util.tex;

import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bibsonomy.util.StringUtils;

/**
 * Framework to encode TeX Macros to unicode.
 * 
 * @author Christian Claus, Dominik Benz
 */
public class TexDecode {

	/**
	 * file holding the mapping between latex macros and unicode codes
	 */
	private static final String LATEXMACRO_UNICODECHAR_MAP_FILENAME = "latex_macro_unicode_char_map.tsv";
	private static final String LATEXMACRO_UNICODECHAR_MAP_DELIM = "\t";
	/**
	 * the mapping between latex macros and unicode codes. It needs to be sorted
	 * because when we build the regex from it, the "longest" macros need to be
	 * present at the beginning of the regex.
	 */
	private static Map<String, String> texMap = new TreeMap<String, String>(new StringLengthComp());
	/** regex patterns */
	private static Pattern texRegexpPattern;
	/** curly brackets regex pattern */
	public static final String CURLY_BRACKETS = "[{}]*";
	/** brackets regex pattern */
	public static final String BRACKETS = "[\\[\\]]*";

	/**
	 * helper comparator to sort strings by their length
	 */
	private static class StringLengthComp implements Comparator<String> {
		@Override
		public int compare(String s1, String s2) {
			if (s1.length() > s2.length()) return -1;
			if (s1.length() < s2.length()) return 1;
			return s1.compareTo(s2);
		}
	}

	/**
	 * initializes the HashMap 'texMap' with TeX macros as key and a referenced
	 * Unicode value as value. Also builds the regex for matching the tex
	 * macros.
	 */
	static {
		loadMapFile();
		final StringBuffer texRegexp = new StringBuffer();
		texRegexp.append("(");
		for (String macro : texMap.keySet()) {
			// build regex
			texRegexp.append(Pattern.quote(macro));
			texRegexp.append("|");
		}
		// delete last "|", add closing bracket
		texRegexp.deleteCharAt(texRegexp.length() - 1);
		texRegexp.append(")");
		// compile pattern
		texRegexpPattern = Pattern.compile(texRegexp.toString());
	}

	/**
	 * Decodes a String which contains TeX macros into it's Unicode
	 * representation.
	 * 
	 * @param s
	 * @return Unicode representation of the String
	 */
	public static String decode(String s) {
		if (s != null) {
			final Matcher texRegexpMatcher = texRegexpPattern.matcher(s.trim().replaceAll(CURLY_BRACKETS, ""));
			final StringBuffer sb = new StringBuffer();
			while (texRegexpMatcher.find()) {
				texRegexpMatcher.appendReplacement(sb, texMap.get(texRegexpMatcher.group()));
			}
			texRegexpMatcher.appendTail(sb);
			return sb.toString().trim().replaceAll(BRACKETS, "");
		}
		return "";
	}

	/**
	 * Getter for the texMap
	 * 
	 * @return Map of TeX->Unicode representation
	 */
	public static Map<String, String> getTexMap() {
		return Collections.unmodifiableMap(texMap);
	}

	/**
	 * parse the file containing the mappings of unicode characters to latex
	 * macros and store it in texMap.
	 */
	private static final void loadMapFile() {
		Scanner scanner = new Scanner(TexDecode.class.getClassLoader().getResourceAsStream(LATEXMACRO_UNICODECHAR_MAP_FILENAME), StringUtils.CHARSET_UTF_8);
		String line;
		String[] parts;
		while (scanner.hasNextLine()) {
			line = scanner.nextLine();
			parts = line.split(LATEXMACRO_UNICODECHAR_MAP_DELIM);
			// convert hex representation into unicode string
			texMap.put(parts[1].trim(), String.valueOf(Character.toChars(Integer.parseInt(parts[0].trim(), 16))));
		}
		scanner.close();
	}

}