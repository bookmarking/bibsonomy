/**
 * BibSonomy-Scrapingservice - Stand-alone web application for web page scrapers (see bibsonomy-scraper)
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.scrapingservice.writers;

import static org.junit.Assert.fail;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.util.PersonNameUtils;
import org.bibsonomy.model.util.PersonNameParser.PersonListParserException;
import org.junit.Test;

public class RDFWriterTest {

	@Test
	public void testWrite() throws PersonListParserException {
		RDFWriter writer;
		try {
			writer = new RDFWriter(new FileOutputStream("/dev/null"));
		} catch (FileNotFoundException e1) {
			fail(e1.getMessage());
			return;
		}
		final BibTex bibtex = new BibTex();
		bibtex.setTitle("Reconsidering Physical Key Secrecy: Teledoplication via Optical Decoding");
		bibtex.setAuthor(PersonNameUtils.discoverPersonNames("Benjamin Laxton and Kai Wand and Stefan Savage"));
		bibtex.setAbstract("The access control provided by a physical lock is based ...");
		bibtex.setBibtexKey("laxton2008reconsidering");
		bibtex.setEntrytype("inproceedings");
		bibtex.setYear("2008");
		bibtex.setMonth("October");
		bibtex.setInstitution("ACM");
		bibtex.setBooktitle("CCS'08");
		bibtex.addMiscField("isbn", "978-1-59593-810-7");
		bibtex.addMiscField("doi", "10.781/978-1-59593-810-7");
		bibtex.setAddress("Alexandria, Virginia, USA");
		bibtex.setUrl("http://portal.acm.org/laxton/2008/reconsidering-physical-key-security");
		
		try {
			writer.write(new URL("http://example.com/laxton/2008/reconsidering").toURI(), bibtex);
		} catch (MalformedURLException e) {
			fail(e.getMessage());
		} catch (URISyntaxException e) {
			fail(e.getMessage());
		}
	}

}
