INSERT INTO `ids` VALUES 
	(0,0,'content_id'),
	(1,0,'tas id'),
	(2,0,'relation id'),
	(3,0,'question id'),
	(4,0,'cycle id'),
	(5,0,'extended_fields_id'),
	(7,0,'scraper_metadata_id'),
	(12,0,'grouptas id'),
	(14,0,'message_id'),
	(15,0,'discussion_id'),
	(16,0,'synchronization_id'),
	(17, 0, 'person_change_id');

INSERT INTO `groupids` (`group_name`, `group`, `privlevel`, `sharedDocuments`) VALUES
	('public', -2147483648,1,0),
	('private',-2147483647,1,0),
	('friends',-2147483646,1,0),
	('public', 0,1,0),
	('private',1,1,0),
	('friends',2,1,0);