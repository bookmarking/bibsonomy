# BibSonomy Opensocial

## What is it?

BibSonomy Opensocial contains the implementation of the Opensocial specification
and classes for OAuth2 authentication.

The module is part of the [BibSonomy](http://www.bibsonomy.org) social
bookmarking system and is maintained by the
[Knowledge & Data Engineering Group](http://www.kde.cs.uni-kassel.de/)
at the University of Kassel, Germany,
the [Data Mining and Information Retrieval Group](http://www.is.informatik.uni-wuerzburg.de/en/dmir/)
at the University of Würzburg, Germany, and
the [L3S Research Center](http://www.l3s.de/)
at Leibniz University Hannover, Germany.

## Documentation

The documentation available as of the date of this release is included in
the form of JavaDoc annotations in the source code.


## Release Notes

Please see the [release log](https://bitbucket.org/bibsonomy/bibsonomy/wiki/documentation/releases/Release%20Log).

## System Requirements

* JDK: 1.7 or above.
* Memory: No minimum requirement.
* Disk: No minimum requirement.
* Operating System: No minimum requirement.

## Licensing

* Please see the file [LICENSE.txt](https://bitbucket.org/bibsonomy/bibsonomy/src/tip/bibsonomy-opensocial/LICENSE.txt?at=stable)

## URLs

* [Home Page](https://bitbucket.org/bibsonomy/bibsonomy)
* [Maven Repository](http://dev.bibsonomy.org/maven2/)
* [Issue Tracking](https://bitbucket.org/bibsonomy/bibsonomy/issues)