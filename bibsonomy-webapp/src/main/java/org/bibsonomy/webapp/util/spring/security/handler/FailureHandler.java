/**
 * BibSonomy-Webapp - The web application for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.webapp.util.spring.security.handler;

import static org.bibsonomy.util.ValidationUtils.present;

import java.io.IOException;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.bibsonomy.webapp.util.RequestLogic;
import org.bibsonomy.webapp.util.TeerGrube;
import org.bibsonomy.webapp.util.spring.security.exceptionmapper.UsernameNotFoundExceptionMapper;
import org.bibsonomy.webapp.util.spring.security.exceptions.UseNotAllowedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

/**
 * @author dzo
 */
public class FailureHandler extends SimpleUrlAuthenticationFailureHandler {
	
	/**
	 * The user that shall be registered is stored in a session attribute with
	 * that name. 
	 */
	public static final String USER_TO_BE_REGISTERED = "register_this_user"; 
	
	private TeerGrube grube;
	private Set<UsernameNotFoundExceptionMapper> usernameNotFoundExceptionMapper;

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
		// TODO: remove instanceof chain
		if (exception instanceof BadCredentialsException) {
			/*
			 * log failure
			 * Brute Force Attacks!! 
			 */
			final BadCredentialsException badCredentialsException = (BadCredentialsException) exception;
			final Authentication authentication = badCredentialsException.getAuthentication();
			final String username = (String) authentication.getPrincipal();
			if (present(username)) {
				final RequestLogic requestLogic = new RequestLogic(request);
				
				this.grube.add(username);
				this.grube.add(requestLogic.getInetAddress());
			}
		}
		/*
		 * redirect to registration (LDAP and OpenID)
		 */
		final RedirectStrategy redirectStrategy = getRedirectStrategy();
		if (exception instanceof UsernameNotFoundException) {
			final UsernameNotFoundException unne = (UsernameNotFoundException) exception;
			/*
			 * Find the correct mapper which handles the specific 
			 * exception (LDAP/OpenID) and "converts" the user data to our 
			 * user object.
			 */
			for (final UsernameNotFoundExceptionMapper mapper : this.usernameNotFoundExceptionMapper) {
				if (mapper.supports(unne)) {
					/*
					 * store user data and authentication in session
					 */
					final HttpSession session = request.getSession(true);
					session.setAttribute(USER_TO_BE_REGISTERED, mapper.mapToUser(unne));
					mapper.writeAdditionAttributes(session, unne);
					
					/*
					 * redirect to the correct registration page
					 */
					redirectStrategy.sendRedirect(request, response, mapper.getRedirectUrl());
					return;
				}
			}
		}
		
		if (exception instanceof UseNotAllowedException) {
			// TODO: use urlgenerator
			redirectStrategy.sendRedirect(request, response, "/authentication/denied/usenotallowed");
			return;
		}
		
		super.onAuthenticationFailure(request, response, exception);
	}

	/**
	 * @param grube the grube to set
	 */
	public void setGrube(TeerGrube grube) {
		this.grube = grube;
	}

	/**
	 * @param usernameNotFoundExceptionMapper
	 */
	public void setUsernameNotFoundExceptionMapper(Set<UsernameNotFoundExceptionMapper> usernameNotFoundExceptionMapper) {
		this.usernameNotFoundExceptionMapper = usernameNotFoundExceptionMapper;
	}
}
