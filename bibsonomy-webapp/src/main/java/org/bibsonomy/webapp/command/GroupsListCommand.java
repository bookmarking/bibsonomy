/**
 * BibSonomy-Webapp - The web application for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.webapp.command;

import static org.bibsonomy.util.ValidationUtils.present;

import java.util.List;

import org.bibsonomy.model.Group;


/**
 * Bean for list of groups.
 * 
 * @author Folke Eisterlehner
 */
public class GroupsListCommand extends BaseCommand {
	private List<Group> list;
	
	// TODO: (bootstrap) remove strAlphabet and alphabet (only used in old layout)
	// dirty hack: alphabet for direct access in group list
	private String strAlphabet = "#ABCDEFGHIJKLMNOPQRSTUVWXYZα"; 
	private char[] alphabet = strAlphabet.toCharArray();
	
	private String format = "html";
	
	/** callback function for JSON outputs */
	private String callback = "";
		
	/**
	 * @return the callback
	 */
	public String getCallback() {
		return this.callback;
	}

	/**
	 * @param callback the callback to set
	 */
	public void setCallback(final String callback) {
		this.callback = callback;
	}
	
	/**
	 * @return The requested format.
	 * 
	 */
	public String getFormat() {
		if (present(this.format)) {
			return this.format;
		}
		
		/*
		 * the default is html
		 * */
		return "html";
	}
	
	/**
	 * @param format
	 */
	public void setFormat(final String format) {
		this.format = format;
	}
	
	/**
	 * stores the data if a new group is requested.
	 */
	private Group requestedGroup;

	/**
	 * @return the alphabet
	 */
	public String getStrAlphabet() {
		return this.strAlphabet;
	}

	/**
	 * @return the alphabet
	 */
	public char[] getAlphabet() {
		return this.alphabet;
	}
	
	/**
	 * @return the sublistlist on the current page
	 */
	public List<Group> getList() {
		return this.list;
	}
	/**
	 * @param list the sublistlist on the current page
	 */
	public void setList(List<Group> list) {
		this.list = list;
	}

	/**
	 * @return the requestedGroup
	 */
	public Group getRequestedGroup() {
		return this.requestedGroup;
	}

	/**
	 * @param requestedGroup the requestedGroup to set
	 */
	public void setRequestedGroup(Group requestedGroup) {
		this.requestedGroup = requestedGroup;
	}	
}