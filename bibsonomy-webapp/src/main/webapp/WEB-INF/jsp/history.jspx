<?xml version="1.0" ?>
<jsp:root version="2.0" xmlns="http://www.w3.org/1999/xhtml"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:layout="urn:jsptagdir:/WEB-INF/tags/layout"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
	xmlns:parts="urn:jsptagdir:/WEB-INF/tags/layout/parts"
	xmlns:user="urn:jsptagdir:/WEB-INF/tags/resources/user"
	xmlns:resource="urn:jsptagdir:/WEB-INF/tags/resources"
	xmlns:bib="urn:jsptagdir:/WEB-INF/tags/resources/bibtex"
	xmlns:pub="urn:jsptagdir:/WEB-INF/tags/resources/publication"
	xmlns:bm="urn:jsptagdir:/WEB-INF/tags/resources/bookmark"
	xmlns:buttons="urn:jsptagdir:/WEB-INF/tags/resources/actionbuttons">

	<jsp:directive.page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" session="true" />
	<layout:paneLayout 
		requPath="${requPath}" 
		command="${command}"
		pageTitle="${command.pageTitle}" 
		headerMessageKey="post.resource.postHistory">

		<jsp:attribute name="headerExt">
			<link rel="stylesheet" type="text/css" href="${resdir}/css/cv_page.css" />
			<script type="text/javascript" src="/resources/javascript/history.js"><!-- keep me --></script>
		</jsp:attribute>

		<!--+
		 	| heading
		 	+-->
		<jsp:attribute name="heading">
			<parts:search pathMessageKey="navi.search" formAction="/search" formInputName="search"/>
		</jsp:attribute>

		<jsp:attribute name="infobox">
			<parts:infobox>
				<jsp:attribute name="infoTextBody">
					<!-- short info about historyPage -->
					<fmt:message key="history.info"/>
					<ul style="list-style-type: circle;padding-left:20px;">
						<li><fmt:message key="history.option1"/></li>
						<li><fmt:message key="history.option2"/></li>
						<li><fmt:message key="history.option3"/></li>
						<li><fmt:message key="history.option4"/></li>
					</ul>
				</jsp:attribute>
			</parts:infobox>
		</jsp:attribute>
		<!--+
			| page content
			+-->
		<jsp:attribute name="content">
			<div class="box align">
				<div class="entry">
					<c:set var="loginUserName" value="${command.context.loginUser.name}"/>
					<c:choose>
						<c:when test="${not empty command.bibtex.list}">
							<c:set var="listView" value="${command.bibtex}"/>
							<c:set var="isPub" value="${true}"/>
							<c:set var="isGoldStandard" value="${false}"/>
						</c:when>
						<c:when test="${not empty command.bookmark.list}">
							<c:set var="listView" value="${command.bookmark}"/>
							<c:set var="isPub" value="${false}"/>
							<c:set var="isGoldStandard" value="${false}"/>
						</c:when>
						<c:when test="${not empty command.goldStandardPublications.list}">
							<c:set var="listView" value="${command.goldStandardPublications}"/>
							<c:set var="isPub" value="${true}"/>
							<c:set var="isGoldStandard" value="${true}"/>
						</c:when>
						<c:when test="${not empty command.goldStandardBookmarks.list}">
							<c:set var="listView" value="${command.goldStandardBookmarks}"/>
							<c:set var="isPub" value="${false}"/>
							<c:set var="isGoldStandard" value="${true}"/>
						</c:when>
					</c:choose>

					<form id="history" method="POST">
						<c:set var="buttonClasses" value="btn btn-primary btn-xs"/>
						<c:set var="fieldlength" value="${fn:length(listView.list)-1}" />
						<!-- this is the current version -->
						<c:set var="newestPost" value="${listView.list[fieldlength]}" />
						<c:set var="newestResource" value="${newestPost.resource}" />
						<c:set var="isGoldStandard" value="${isGoldStandard}" />

						<!-- hidden variables are used to communicate with .js file or controllers -->
						<input type="hidden" name="newestPost" value="${newestPost}" />
						<input type="hidden" name="differentEntryKeys" />
						<input type="hidden" name="compareVersion" />
						<input type="hidden" name="listLength" value="${fieldlength}"/>
						<input type="hidden" name="isPub" value="${isPub}" />
						<input type="hidden" name="isGoldStandard" value="${isGoldStandard}" />

						<fmt:message key="history.restoreBtn.disabled" var="restoreBtnDisabledMsg" />
						<fmt:message key="history.restoreBtn.enabled" var="restoreBtnEnabledMsg" />
						<fmt:message key="goldStandard.approved.title" var="approved"/>
						<fmt:message key="goldStandard.approved.revision.title" var="approved_revision"/>
						<fmt:message key="history.backToPost" var="backToPostBtnMsg" />
						
						<!--+
							| complete Post
							+-->
						<div class="title">
							<c:choose>
								<c:when test="${isPub}">
									<!-- TODO: remove copy paste code -->
									<c:set var="itemType" value="http://schema.org/ScholarlyArticle"/>
									<c:if test="${not empty newestResource.entrytype and ('book' eq newestResource.entrytype)}">
										<c:set var="itemType" value="http://schema.org/Book"/>
									</c:if>
									<c:choose>
										<c:when test="${isGoldStandard}">
											<input type="hidden" name="intraHashToUpdate" value="${newestResource.interHash}"/>
											
											<div class="title" itemtype="${itemType}" itemscope="itemscope">
												<div class="media">
													<div class="media-body" id="titletext">
														<div class="pull-right"><buttons:communityPostActions post="${newestPost}" resourceType="bibtex" communityPostExists="${true}" /></div>
														<h1 class="media-heading" id="pubtitle"><c:out value="${mtl:cleanBibtex(newestPost.resource.title)}" />
															<c:if test="${newestPost.approved}">
																<a title="${fn:escapeXml(approved)}">
																	<span class="fa fa-check-circle" style="margin-left:6px;"><!--  --></span>
																</a>
															</c:if>
														</h1>
														<p><bib:desc publication="${newestResource}"/></p>
													</div>
												</div>
											</div>
										</c:when>

										<c:otherwise>
											<input type="hidden" name="intraHashToUpdate" value="${newestPost.resource.intraHash}"/>
											<div class="title" itemtype="${itemType}" itemscope="itemscope">
												<pub:title post="${newestPost}">
													<jsp:attribute name="pubActionButtons">
														<div class="pull-right">
															<buttons:actions post="${newestPost}" loginUserName="${command.context.loginUser.name}" disableResourceLinks="${false}" resourceType="bibtex"/>
														</div>
													</jsp:attribute>
												</pub:title>
											</div>
										</c:otherwise>
									</c:choose>
								</c:when>

								<c:otherwise>
									<c:choose>
										<c:when test="${isGoldStandard}">
											<input type="hidden" name="intraHashToUpdate" value="${newestPost.resource.interHash}"/>
											<div class="title">
												<div class="media">
													<div class="media-body" id="titletext">
														<div class="pull-right"><buttons:communityPostActions post="${newestPost}" resourceType="bookmark" communityPostExists="${true}" /></div>
														<h1 class="media-heading" id="pubtitle" itemprop="name"><c:out value="${mtl:cleanBibtex(newestPost.resource.title)}" /></h1>
														<c:set var="url" value="${newestPost.resource.url}" />
														<p><a href="${url}" itemprop="url"><c:out value="${url}" /></a></p>
													</div>
												</div>
											</div>
										</c:when>

										<c:otherwise>
											<input type="hidden" name="intraHashToUpdate" value="${newestPost.resource.intraHash}"/>
											<div class="title">
												<div class="media">
												<div class="media-body" id="titletext">
													<div class="pull-right"><buttons:actions post="${newestPost}" loginUserName="${command.context.loginUser.name}" resourceType="bookmark"/></div>
													<h1 class="media-heading" id="pubtitle" itemprop="name"><c:out value="${mtl:cleanBibtex(newestPost.resource.title)}" /></h1>
													<c:set var="url" value="${newestPost.resource.url}" />
													<p><a href="${url}"><c:out value="${url}" /></a></p>
												</div>
												</div>
											</div>
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							</c:choose>
						</div>
						
						<a id="backToPostBtn" class="${buttonClasses} visible" 
									title="${backToPostBtnMsg}">
									<fmt:message key="history.backToPost"/>
						</a>
						
						<div id="historyBorder">
							<span> </span>
						</div> <!-- border -->


						<c:choose>
							<c:when test="${fn:length(listView.list) gt 1}">
								<!--+
									| history list
									+-->
									
								<table class="table table-striped">

									<thead> <!-- history list's header info -->
										<tr>
											<th colspan="5">
												<div class="alert alert-info" style="margin-bottom:0px;" role="alert">
													<fmt:message key="history.historyList.header" />
												</div>
											</th>
										</tr>
									</thead>

									<tbody>
										<c:forEach var="index" begin="0" end="${fieldlength}" varStatus="loopStatus" step="1">
											<c:set var="historicalPost" value="${listView.list[(fieldlength-index)]}"/>
											<c:set var="additionalItemClasses" value="${loopStatus.index % 2 == 0 ? 'odd' : 'even'}" />
											<tr>
												<!-- version number -->
												<td class="col-sm-1"  id="versionNum" style="border-right:1px solid #eee">
													<span class="label label-default" style="vertical-align:bottom;padding:8px;">
														Ver. ${fieldlength-index}
														<c:if test="${listView.list[(fieldlength-index)].approved}">
															<a title="${fn:escapeXml(approved_revision)}">
																<span class="fa fa-check-circle" style="vertical-align:bottom;padding-left:3px;"><!--  --></span>
															</a>
														</c:if>
													</span>
												</td>
												
												<!-- change date -->
												<td class="col-sm-3" style="border-right:1px solid #eee">
													<fmt:formatDate var="changeDate" type="both" dateStyle="long" value="${historicalPost.changeDate}"/>
													<span class="label label-primary diffNumText" style="vertical-align:bottom;padding:8px;" title="edited on ${changeDate}">${changeDate}</span>
												</td>
												
												<!-- user name -->
												<td class="col-sm-1">
													<c:if test="${isGoldStandard}">
														<div style="float:left" >
															<h6><user:username user="${historicalPost.user}"/></h6>
														</div>
													</c:if>
												</td>
												
												<!-- diff to previous version -->
												<c:choose>
													<!-- if version 0  -->
													<c:when test="${index eq fieldlength }">
														<c:set var="diffArrayPre" value="${null}"/>
													</c:when>
													<c:otherwise>
														<c:set var="diffArrayPre" value="${mtl:diffEntries(listView.list[(fieldlength-index-1)],historicalPost)}"/>
													</c:otherwise>
												</c:choose>
	
												<c:set var="diffArrayCurr" value="${mtl:diffEntries(newestPost,historicalPost)}"/>									

												<!-- number of changed fields, in comparison to the previous version -->
												<c:set var="diffNumPre" value="${fn:length(diffArrayPre)}"/>
								
												<!-- number of changed fields, in comparison to the current version -->
												<c:set var="diffNumCurr" value="${fn:length(diffArrayCurr)}"/>
								
												<!-- select revision to compare -->
												<td class="col-sm-6">	
													<div class="col-sm-4" style="float:left">
														<h6> <label class="control-label" style="white-space:nowrap;">
															<fmt:message key="history.historySelect.compareTo" />
														</label> </h6>
													</div>
													<div class="col-sm-8" style="float:left">
														<select id="preCurrSelector" class="form-control input-sm help-popover">
															<option value="0">
																<fmt:message key="history.selection.previous"> version (${diffNumPre} different fields)
																	<fmt:param>${diffNumPre}</fmt:param>
																</fmt:message>
															</option>
															<option value="1">
																<fmt:message key="history.selection.current">
																	<fmt:param>${diffNumCurr}</fmt:param>
																	current version (${diffNumCurr} different fields)
																</fmt:message>
															</option>
														</select>
														<div class="help help-header hide"><!--  --></div>
														<div class="help help-content hide"><fmt:message key="history.selection.help" /></div>
													</div>
												</td>

												<!-- restore button -->
												<td class="col-sm-1">
													<a id="restoreBtnDisabled" class="${buttonClasses} invisible" style="vertical-align:bottom;padding:4px;" 
														title="${restoreBtnDisabledMsg}" disabled="disabled">
														<fmt:message key="restore"/>
													</a>
													<a id="restoreBtnEnabled" class="${buttonClasses} invisible"  style="vertical-align:bottom;padding:4px;" 
														title="${restoreBtnEnabledMsg}">
														<fmt:message key="restore"/>
														<!-- <span class="fa fa-undo"> --><!--  --><!-- </span> -->
													</a>
												</td>
											</tr>
							
											<!-- second row -->
											<tr>
												<!-- comparison to the previous version -->

												<td colspan="5" id="postDiffPre" class="postDiffPre">
													<c:choose>	
														<c:when test="${index eq fieldlength }">
															<div class="alert alert-info" style="margin-bottom:0px;" role="alert">
																<fmt:message key="history.firstVersion" />
															</div>
														</c:when>
														<c:when test="${fn:length(diffArrayPre) eq 0}">
															<!-- we are dealing with a post which is identical to the previous version -->
															<div id = "preVer" class="alert alert-info " style="margin-bottom:0px;" role="alert">
																<fmt:message key="history.identicalToPreVersion" />
															</div>
														</c:when>
													</c:choose>
													<dl class="dl-horizontal dl-history">
														<c:forEach var="diffEntry" items="${diffArrayPre}">
															<dt><fmt:message key="post.resource.${diffEntry.key}"/></dt>
															<dd>${diffEntry.value}</dd>
														</c:forEach>
													</dl>
									
												</td>
												<!-- comparison to the current version -->
												<td colspan="5" id="postDiffCurr" class="postDiffCurr invisible">
													<c:if test="${fn:length(diffArrayCurr) eq 0}">
														<!-- we are dealing with a post which is identical to the latest version (current post) -->
														<div id = "currentVer" class="alert alert-info " style="margin-bottom:0px;" role="alert">
															<fmt:message key="history.currentVersion" />
														</div>
													</c:if>	
													<dl class="dl-horizontal dl-history">
														<c:forEach var="diffEntry" items="${diffArrayCurr}">
															<dt>
																<input type="checkbox" id="CurrEntryCheckbox" class="invisible" checked="checked"/><c:out value=" "/>
																<fmt:message key="post.resource.${diffEntry.key}"/>
															</dt>
															<dd>${diffEntry.value}</dd>
															<div>
																<!-- the following two variables are used in .js file -->
																<input type="hidden" name="diffEntryKey" value="${diffEntry.key}"/>
																<input type="hidden" name="compareVersion" value="${fieldlength-index}"/>
															</div>
														</c:forEach>
													</dl>
													<!-- restore confirm (alert) question -->
													<div id="restore_alert_btn" class="invisible hidden">
														<div class="alert col-sm-10 alert-warning" style="margin-bottom:0px;padding:6px 12px;" role="alert">
															<fmt:message key="history.restore.confirm" />
														</div>
														<!-- submit restore -->
														<button class="btn btn-primary submitBtn" style="float:right"><fmt:message key="restore"/></button>
													</div>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</c:when>
							<c:otherwise>
								<div class="alert alert-info" style="margin:10px;" role="alert">
									<fmt:message key="History.empty" />
								</div>
							</c:otherwise>
						</c:choose>
					</form>
				</div>
			</div>
		</jsp:attribute>
	</layout:paneLayout>
</jsp:root>
