<?xml version="1.0" ?>
<jsp:root version="2.0" xmlns="http://www.w3.org/1999/xhtml" 
	xmlns:jsp="http://java.sun.com/JSP/Page" 
	xmlns:c="http://java.sun.com/jsp/jstl/core" 
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:layout="urn:jsptagdir:/WEB-INF/tags/layout" 
	xmlns:tags="urn:jsptagdir:/WEB-INF/tags/tags" 
	xmlns:bm="urn:jsptagdir:/WEB-INF/tags/resources/bookmark"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions" 
	xmlns:rc="urn:jsptagdir:/WEB-INF/tags/resources/common" 
	xmlns:batch="urn:jsptagdir:/WEB-INF/tags/actions/edit/batch"
	xmlns:parts="urn:jsptagdir:/WEB-INF/tags/layout/parts" 
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld">

	<!-- the skeleton for the batchedit pages (batcheditbib, batchediturl) -->
	<jsp:directive.attribute name="listView" type="org.bibsonomy.webapp.command.ListCommand" required="true" />
	<jsp:directive.attribute name="listViewStartParamName" type="java.lang.String" required="true" />
	<jsp:directive.attribute name="otherPostsUrlPrefix" type="java.lang.String" required="true" />
	<jsp:directive.attribute name="updateExistingPost" type="java.lang.Boolean" required="false" />
	<jsp:directive.attribute name="overwrite" type="java.lang.Boolean" required="false" />
	<jsp:directive.attribute name="resourceType" type="java.lang.String" required="true" description="The type of resource that are edited (bookmark or bibtex)." />

	<jsp:directive.attribute name="shortPostDescription" fragment="true" required="true" description="A one line description of a post" />

	<jsp:directive.variable name-given="post" scope="NESTED" />

	<!-- page title -->
	<fmt:message key="post.meta.edit" var="pageTitle" />

	<layout:paneLayout requPath="${requPath}" command="${command}" pageTitle="${pageTitle}" activeTab="my" headerMessageKey="batchedit.info" showPageOptions="${true}">
		<jsp:attribute name="infobox">
			<parts:infobox>
				<jsp:attribute name="infoTextBody">
					<!-- short info about batchEdit -->
					<fmt:message key="batchedit.info1"/>
					<ul style="list-style: square outside none;padding: 10px 10px 10px 25px;">
						<c:if test="${resourceType eq 'bibtex'}">
							<li><fmt:message key="batchedit.option1"/></li>
						</c:if>
						<li><fmt:message key="batchedit.option2"/></li>
						<li><fmt:message key="batchedit.option3"/></li>
						<li><fmt:message key="batchedit.option4"/></li>
						<li><fmt:message key="batchedit.option5"/></li>
					</ul>
					<ul><fmt:message key="batchedit.info2"/></ul>
				</jsp:attribute>
			</parts:infobox>
		</jsp:attribute>
		
		<jsp:attribute name="heading">
			<parts:search pathMessageKey="post.meta.edit" />
		</jsp:attribute>


		<jsp:attribute name="content">
			<h1 class="content-heading"><fmt:message key="post.meta.batch_edit" /></h1>
			<batch:content resourceType="${resourceType}" listView="${listView}" 
				otherPostsUrlPrefix="${otherPostsUrlPrefix}" directEdit="${true}" 
				listViewStartParamName="${listViewStartParamName}" 
				updateExistingPost="${updateExistingPost}" overwrite="${overwrite}">
				<jsp:attribute name="shortPostDescription">
					<jsp:invoke fragment="shortPostDescription" />
				</jsp:attribute>
			</batch:content> 
		</jsp:attribute>
	</layout:paneLayout>
</jsp:root>