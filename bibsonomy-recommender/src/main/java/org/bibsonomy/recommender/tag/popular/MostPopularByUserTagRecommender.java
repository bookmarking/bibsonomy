/**
 * BibSonomy Recommendation - Tag and resource recommender.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.recommender.tag.popular;

import java.util.Collection;
import java.util.List;

import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;
import org.bibsonomy.recommender.tag.AbstractTagRecommender;
import org.bibsonomy.recommender.tag.model.RecommendedTag;
import org.bibsonomy.recommender.tag.service.RecommenderMainTagAccess;

import recommender.core.model.Pair;

/**
 * Returns the most popular (i.e., most often used) tags of the user as 
 * recommendation for the entity.  
 * 
 * @author fei
 */
public class MostPopularByUserTagRecommender extends AbstractTagRecommender {
	
	private RecommenderMainTagAccess dbAccess;
	
	@Override
	protected void addRecommendedTagsInternal(final Collection<RecommendedTag> recommendedTags, final Post<? extends Resource> entity) {
		final String username = entity.getUser().getName();
		if (username != null) {
			
			/*
			 * we get the count to normalize the score
			 */
			final int count = this.dbAccess.getNumberOfTaggingsForUser(username);
			
			final List<Pair<String, Integer>> tagsWithCount = this.dbAccess.getMostPopularTagsForUser(username, this.numberOfTagsToRecommend);
			for (final Pair<String, Integer> tagWithCount : tagsWithCount) {
				final String tag = this.getCleanedTag(tagWithCount.getFirst());
				if (tag != null) {
					recommendedTags.add(new RecommendedTag(tag, ((1.0 * tagWithCount.getSecond().doubleValue()) / count), 0.5));
				}
			}
		}
	}

	@Override
	public String getInfo() {
		return "Most Popular Tags By User Recommender";
	}

	@Override
	protected void setFeedbackInternal(final Post<? extends Resource> entity, final RecommendedTag tag) {
		/*
		 * this recommender ignores feedback
		 */
	}
	
	public void setDbAccess(RecommenderMainTagAccess dbAccess) {
		this.dbAccess = dbAccess;
	}
}
