/**
 * BibSonomy-Rest-Common - Common things for the REST-client and server.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.rest.validation;

import org.bibsonomy.rest.renderer.xml.BibtexType;
import org.bibsonomy.rest.renderer.xml.BookmarkType;
import org.bibsonomy.rest.renderer.xml.GroupType;
import org.bibsonomy.rest.renderer.xml.PostType;
import org.bibsonomy.rest.renderer.xml.TagType;
import org.bibsonomy.rest.renderer.xml.UserType;

/**
 * interface for validating the xml input from the API
 * 
 * @author dzo
 */
public interface XMLModelValidator {

	/**
	 * @param xmlPublication
	 */
	public void checkPublicationXML(BibtexType xmlPublication);

	/**
	 * @param xmlBookmark
	 */
	public void checkBookmarkXML(BookmarkType xmlBookmark);

	/**
	 * @param xmlPost
	 */
	public void checkPost(PostType xmlPost);

	/**
	 * @param xmlTag
	 */
	public void checkTag(TagType xmlTag);

	/**
	 * @param xmlGroup
	 */
	public void checkGroup(GroupType xmlGroup);

	/**
	 * @param xmlUser
	 */
	public void checkUser(UserType xmlUser);

	/**
	 * @param xmlPost
	 */
	public void checkStandardPost(PostType xmlPost);

}
