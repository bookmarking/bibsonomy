# BibSonomy

[BibSonomy](https://www.bibsonomy.org/) is a social bookmark and publication sharing system. 

It is developed and operated by 
the [KDE group](http://www.kde.cs.uni-kassel.de/contact.html) of the University of Kassel, 
the [DMIR group](http://www.dmir.uni-wuerzburg.de/) of the University of Würzburg, and 
the [L3S Research Center](http://www.l3s.de/), Germany.


More information can be found in the [BibSonomy Wiki](https://bitbucket.org/bibsonomy/bibsonomy/wiki/Home).
