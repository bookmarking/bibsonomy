/**
 * BibSonomy-Web-Common - Common things for web
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.util.id;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bibsonomy.util.WebUtils;
import org.junit.Ignore;
import org.junit.Test;

/**
 * @author rja
 */
public class DOIUtilsTest {

	private static final Random rand = new Random();

	private static final String bibtexWithDoi = "@article{Pevzner02,\n" + 
	"address = {Cambridge, MA, USA},\n" +
	"author = {Lev Pevzner and Marti A. Hearst},\n" +
	"interHash = {707f8d31137f6d39edd1d54664351d0d},\n" +
	"intraHash = {330a11348556280054ec5fceeb23649c},\n" +
	"journal = {Comput. Linguist.},\n" +
	"number = {1},\n" +
	"pages = {19--36},\n" +
	"publisher = {MIT Press},\n" +
	"title = {A critique and improvement of an evaluation metric for text segmentation},\n" +
	"url = {http://portal.acm.org/citation.cfm?id=636737&dl=GUIDE&coll=GUIDE&CFID=27698866&CFTOKEN=51312152},\n" +
	"volume = {28},\n" +
	"year = {2002},\n" +
	"issn = {0891-2017},\n" +
	"doi = {http://dx.doi.org/10.1162/089120102317341756},\n" + 
	"}";
	
	private static final String bibtexWithCleanDoi = "@article{Pevzner02,\n" + 
	"address = {Cambridge, MA, USA},\n" +
	"author = {Lev Pevzner and Marti A. Hearst},\n" +
	"interHash = {707f8d31137f6d39edd1d54664351d0d},\n" +
	"intraHash = {330a11348556280054ec5fceeb23649c},\n" +
	"journal = {Comput. Linguist.},\n" +
	"number = {1},\n" +
	"pages = {19--36},\n" +
	"publisher = {MIT Press},\n" +
	"title = {A critique and improvement of an evaluation metric for text segmentation},\n" +
	"url = {http://portal.acm.org/citation.cfm?id=636737&dl=GUIDE&coll=GUIDE&CFID=27698866&CFTOKEN=51312152},\n" +
	"volume = {28},\n" +
	"year = {2002},\n" +
	"issn = {0891-2017},\n" +
	"doi = {10.1162/089120102317341756},\n" + 
	"}";
	
	private static final String httpDoi = "http://dx.doi.org/10.1162/089120102317341756";
	
	private static final String urlWithDoi = "url={http://dx.doi.org/10.1007/11762256_31},";
	
	private static final String bibtexLineWithDoi = "doi={http://dx.doi.org/10.1007/11762256_31},";
	private static final String cleanBibtexLineWithDoi = "doi={10.1007/11762256_31},";

	
	private static final String bibtexDoi = "10.1162/089120102317341756";

	private static final String[] dois = new String[] 
	                                                {
		"10.1016/j.cemconres.2003.10.011",
		"doi:10.1016/j.cemconres.2003.10.011",
		"10.1023/A:1009769707641",
		"10.1016/S0169-7552(98)00110-X",
		"10.1145/860451",
		"10.1002/cpe.607",
		"doi:10.1109/ISSTA.2002.1048560",
		"DOI: 10.1016/j.spl.2008.05.017",
		"10.1145/160688.160713"
	                                                };

	private static final String[] nonDois = new String[] 
	                                                   {
		" 10.1016/j.cemconres.2003.10.011 ",
		"10.1016/j.cemconres.2003.10.011\"",
		"10.1016/j.cemconres.2003.10.011'",
		null,
		"",
		"ysdfklaskld",
		"10.1016/j.cemconres.2003.10.011 asef",
		"\n10.1016/j.cemconres.2003.10.}011"
	                                                   };

	private static final String[] fuzzyStarts = new String[] {
		"  ",
		":",
		"\"",
		"'",
		"("
	};

	private static final String[] fuzzyEndings = new String[] {
		" ",
		"\"sadfasdf",
		"'"
	};



	private static final String[] fuzzyDoiOnlyStarts = new String[] {
		"  ",
		":",
		"\"",
		"'",
		"("
	};

	private static final String[] fuzzyDoiOnlyEndings = new String[] {
		" ",
		"\"",
		"'"
	};

	private static final Pattern DOI_START = Pattern.compile("^doi:\\s*(.*)", Pattern.CASE_INSENSITIVE);

	@Test
	public void testIsDOIURL() {
		for(final String doi: dois) {
			try {
				assertTrue(DOIUtils.isDOIURL(DOIUtils.getURL(doi)));
			} catch (MalformedURLException ex) {
				fail(ex.getMessage());
			}
		}
	}


	private String fuzzifyDoiOnlyDOI(final String doi) {
		return 
		fuzzyDoiOnlyStarts[rand.nextInt(fuzzyDoiOnlyStarts.length)] + 
		doi + 
		fuzzyDoiOnlyEndings[rand.nextInt(fuzzyDoiOnlyEndings.length)];
	}

	private String fuzzifyDOI(final String doi) {
		return 
		fuzzyStarts[rand.nextInt(fuzzyStarts.length)] + 
		doi + 
		fuzzyEndings[rand.nextInt(fuzzyEndings.length)];
	}

	private String stripDOI(final String doi) {
		final Matcher matcher = DOI_START.matcher(doi);
		if (matcher.find()) {
			return matcher.group(1);
		}
		
		return doi;
	}

	@Test
	public void testExtractDOI() {
		for(final String doi: dois) {
			final String fuzzyDoi = fuzzifyDOI(doi);
			assertEquals(stripDOI(doi), DOIUtils.extractDOI(fuzzyDoi));
		}
		assertEquals(bibtexDoi, DOIUtils.extractDOI(bibtexWithDoi));
	}

	@Test
	public void testContainsOnlyDOI() {
		for(final String doi: dois) {
			final String fuzzyDoi = fuzzifyDoiOnlyDOI(doi);
			assertTrue(DOIUtils.containsOnlyDOI(doi));
			assertTrue(DOIUtils.containsOnlyDOI(fuzzyDoi));
		}
		assertFalse(DOIUtils.containsOnlyDOI(bibtexWithDoi));
	}

	@Test
	public void testContainsDOI() {
		for(final String doi: dois) {
			assertTrue(DOIUtils.containsDOI(doi));
			assertTrue(DOIUtils.containsDOI(fuzzifyDOI(doi)));
		}
		assertTrue(DOIUtils.containsDOI(bibtexWithDoi));
	}


	@Test
	public void testIsDOI() {
		for (final String doi: dois) {
			assertTrue(DOIUtils.isDOI(doi));
		}
		assertFalse(DOIUtils.isDOI(bibtexWithDoi));
	}

	@Test
	public void testIsNonDOI() {
		for (final String doi: nonDois) {
			assertFalse(DOIUtils.isDOI(doi));
		}
		for (final String doi: dois) {
			assertFalse(DOIUtils.isDOI(fuzzifyDOI(doi)));
		}
	}
	
	@Test
	public void testCleanDOI() {
		for (final String s : dois) {
			if (s.contains("doi")) {
				assertEquals(true, DOIUtils.cleanDOI(s).contains("doi"));
			} else if (s.contains("DOI")) {
				assertEquals(true, DOIUtils.cleanDOI(s).contains("DOI"));
			}
		}

		assertEquals(bibtexWithCleanDoi, DOIUtils.cleanDOI(bibtexWithDoi));
		assertEquals(urlWithDoi, DOIUtils.cleanDOI(urlWithDoi));
		assertEquals(cleanBibtexLineWithDoi, DOIUtils.cleanDOI(bibtexLineWithDoi));
	}
	
	@Test
	public void testExtract2() throws Exception {
		assertEquals("10.1109/ISSTA.2002.1048560", DOIUtils.extractDOI("doi = {doi:10.1109/ISSTA.2002.1048560}"));
		assertEquals("10.1007/11762256_31", DOIUtils.extractDOI("doi={http://dx.doi.org/10.1007/11762256_31}"));
		assertEquals("10.1007/11762256_31", DOIUtils.extractDOI("http://dx.doi.org/10.1007/11762256_31"));
		assertEquals("10.1109/ISSTA.2002.1048560", DOIUtils.extractDOI("doi:10.1109/ISSTA.2002.1048560"));
		assertEquals("10.1109/ISSTA.2002.1048560", DOIUtils.extractDOI("10.1109/ISSTA.2002.1048560"));		
	}


	/**
	 * test getting URL
	 * FIXME: Thomas, kannst Du bitte mal schauen, was hier schiefläuft? Ist ja total seltsam ...
	 * 
	 * Im Browser komme ich bei Aufruf von
	 * 
	 * http://dx.doi.org/10.1007/11922162
	 * 
	 * auf 
	 * 
	 * http://www.springerlink.com/content/w425794t7433/
	 * 
	 * raus. Aber {@link WebUtils#getRedirectUrl(URL)} kommt auf 
	 * 
	 * http://www.springerlink.com/link.asp?id=w425794t7433
	 * 
	 * raus. Auch wenn ich 
	 * 
	 * http://www.springerlink.com/index/10.1007/11922162
	 * 
	 * (das ist die alte URL aus dem Test hier) eingebe, komme ich auf 
	 * der ersten URL raus. D.h., irgendwie scheint Springer da je nach
	 * Cookie-Handling, Referer, oder nach Browser woanders hinzuleiten. :-(
	 * 
	 *   
	 */
	@Test
	@Ignore
	public void getUrlForDoiTest(){
		assertEquals("http://www.springerlink.com/link.asp?id=w425794t7433", DOIUtils.getUrlForDoi("10.1007/11922162").toString());
	}
	
	@Test
	public void testGetDoiFromURL() throws MalformedURLException {
		URL testURL;
		testURL = new URL(
				"http://journals.sagepub.com/doi/abs/10.1177/0165551512438353#articleCitationDownloadContainer");
		String doi = DOIUtils.getDoiFromURL(testURL);
		assertEquals("10.1177/0165551512438353", doi);
	}
	
	@Test
	@Ignore // remote test
	public void testWebPage() throws IOException {
		URL testURL;
		testURL = new URL("https://link.springer.com/book/10.1007/978-3-319-60492-3#about");
		String doi = DOIUtils.getDoiFromWebPage(testURL);
		assertEquals("10.1007/978-3-319-60492-3", doi);
	}
}
