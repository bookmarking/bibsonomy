# BibSonomy Scraper

## What is it?

The BibSonomy scrapers facilitate the extraction of bibliographic
metadata from web pages. The receive as input a URL or a short piece
of text and try to find the metadata of the publication described at
the URL or within the text snippet. The output is provided as a
[BibTeX entry](https://en.wikipedia.org/wiki/BibTeX) which can be
parsed using the [BibTeX parser](../bibsonomy-bibtex-parser).

The module is part of the [BibSonomy](http://www.bibsonomy.org) social
bookmarking system and is maintained by the
[Knowledge & Data Engineering Group](http://www.kde.cs.uni-kassel.de/)
at the University of Kassel, Germany, the
[Data Mining and Information Retrieval Group](http://www.is.informatik.uni-wuerzburg.de/en/dmir/)
at the University of Würzburg, Germany, and the
[L3S Research Center](http://www.l3s.de/) at Leibniz University
Hannover, Germany.

## Documentation

The
[BibSonomy wiki page](https://bitbucket.org/bibsonomy/bibsonomy/wiki/development/modules/scraper/Scraper)
provides a good overview on the structure and functionality of the
scrapers. More documentation is included in the form of JavaDoc
annotations in the source code. A
[list of active scrapers](https://www.bibsonomy.org/scraperinfo) is
provided in BibSonomy. The scrapers can also be tested using
BibSonomy's [scraping service](http://scraper.bibsonomy.org/).


## Release Notes

Please see the [release log](https://bitbucket.org/bibsonomy/bibsonomy/wiki/documentation/releases/Release%20Log).


## System Requirements

* JDK: 1.7 or above.
* Memory: No minimum requirement.
* Disk: No minimum requirement.
* Operating System: No minimum requirement.

## Licensing

* Please see the file [LICENSE.txt](https://bitbucket.org/bibsonomy/bibsonomy/src/tip/bibsonomy-scraper/LICENSE.txt?at=stable)


## Maven URLS

* [Home Page](https://bitbucket.org/bibsonomy/bibsonomy)
* [Maven Repository](http://dev.bibsonomy.org/maven2/)
* [Issue Tracking](https://bitbucket.org/bibsonomy/bibsonomy/issues)
