/**
 * BibSonomy-Scraper - Web page scrapers returning BibTeX for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.scraper.url.kde.plos;

import static org.bibsonomy.scraper.junit.RemoteTestAssert.assertScraperResult;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.URL;

import org.bibsonomy.scraper.ScrapingContext;
import org.bibsonomy.scraper.junit.RemoteTest;
import org.junit.Test;
import org.junit.experimental.categories.Category;


/**
 * Scraper URL tests #43 & #44 for PlosScraper
 * 
 * TODO:
 * This test works only on Java in 64bit version. The problem is a regex
 * in the endote converter which thorws a StackOverFlowException, because of
 * the huge abstracts (it seems that any citation on plos.org has large abstracts).
 * 
 * @author tst
 */
@Category(RemoteTest.class)
public class PlosScraperTest {
	
	/**
	 * starts URL test with id url_43
	 */
	@Test
	public void urlTest1Run(){
		final String url = "http://www.plosbiology.org/article/info:doi/10.1371/journal.pbio.0060010";
		final String resultFile = "PlosScraperUnitURLTest1.bib";
		assertScraperResult(url, null, PlosScraper.class, resultFile);
	}
	
	/**
	 * starts URL test with id url_44
	 */
	@Test
	public void urlTest2Run(){
		final String url = "http://www.plosbiology.org/article/citationList.action?articleURI=info:doi/10.1371/journal.pbio.0060010";
		final String resultFile = "PlosScraperUnitURLTest1.bib";
		assertScraperResult(url, null, PlosScraper.class, resultFile);
	}
	
	/**
	 * starts URL test with id url_172
	 */
	@Test
	public void urlTest3Run(){
		final String url = "http://www.plosmedicine.org/article/info:doi/10.1371/journal.pmed.1000248";
		final String resultFile = "PlosScraperUnitURLTest2.bib";
		assertScraperResult(url, null, PlosScraper.class, resultFile);
	}
	
	/**
	 * starts URL test with id url_173
	 */
	@Test
	public void urlTest4Run(){
		final String url = "http://www.plosmedicine.org/article/getBibTexCitation.action?articleURI=info:doi/10.1371/journal.pmed.1000248";
		final String resultFile = "PlosScraperUnitURLTest2.bib";
		assertScraperResult(url, null, PlosScraper.class, resultFile);
	}
	
	/**
	 * starts URL test with id url_200
	 */
	@Test
	public void urlTest5Run(){
		final String url = "http://www.plosbiology.org/article/info%3Adoi%2F10.1371%2Fjournal.pbio.1001148";
		final String resultFile = "PlosScraperUnitURLTest3.bib";
		assertScraperResult(url, null, PlosScraper.class, resultFile);
	}
	
	/**
	 * starts URL test with id url_201
	 */
	@Test
	public void urlTest6Run(){
		final String url = "http://www.plosmedicine.org/article/info%3Adoi%2F10.1371%2Fjournal.pmed.1001094";
		final String resultFile = "PlosScraperUnitURLTest4.bib";
		assertScraperResult(url, null, PlosScraper.class, resultFile);
	}
	
	/**
	 * starts URL test with id url_202
	 */
	@Test
	public void urlTest7Run(){
		final String url = "http://www.ploscompbiol.org/article/info%3Adoi%2F10.1371%2Fjournal.pcbi.1002146";
		final String resultFile = "PlosScraperUnitURLTest5.bib";
		assertScraperResult(url, null, PlosScraper.class, resultFile);
	}
	
	/**
	 * starts URL test with id url_203
	 */
	@Test
	public void urlTest8Run(){
		final String url = "http://www.plosgenetics.org/article/info%3Adoi%2F10.1371%2Fjournal.pgen.1002285";
		final String resultFile = "PlosScraperUnitURLTest6.bib";
		assertScraperResult(url, null, PlosScraper.class, resultFile);
	}
	
	/**
	 * starts URL test with id url_204
	 */
	@Test
	public void urlTest9Run(){
		final String url = "http://www.plospathogens.org/article/info%3Adoi%2F10.1371%2Fjournal.ppat.1002253";
		final String resultFile = "PlosScraperUnitURLTest7.bib";
		assertScraperResult(url, null, PlosScraper.class, resultFile);
	}
	
	/**
	 * starts URL test with id url_205
	 */
	@Test
	public void urlTest10Run(){
		final String url = "http://www.plosone.org/article/info%3Adoi%2F10.1371%2Fjournal.pone.0025007";
		final String resultFile = "PlosScraperUnitURLTest8.bib";
		assertScraperResult(url, null, PlosScraper.class, resultFile);
	}
	
	/**
	 * starts URL test with id url_206
	 */
	@Test
	public void urlTest11Run(){
		final String url = "http://www.plosntds.org/article/info%3Adoi%2F10.1371%2Fjournal.pntd.0001305";
		final String resultFile = "PlosScraperUnitURLTest9.bib";
		assertScraperResult(url, null, PlosScraper.class, resultFile);
	}
	
	/**
	 * starts URL test with id url_284
	 */
	@Test
	public void urlTest12Run(){
		final String url = "http://www.plosone.org/article/info%3Adoi%2F10.1371%2Fjournal.pone.0114825";
		final String resultFile = "PlosScraperUnitURLTest10.bib";
		assertScraperResult(url, null, PlosScraper.class, resultFile);
	}
	
	/**
	 * starts URL test with id url_341
	 */
	@Test
	public void urlTest13Run(){
		final String url = "http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0089052";
		final String resultFile = "PlosScraperUnitURLTest11.bib";
		assertScraperResult(url, null, PlosScraper.class, resultFile);
	}
	
	/**
	 * @throws Exception
	 */
	@Test
	public void testReferences() throws Exception{
		final ScrapingContext sc = new ScrapingContext(new URL("http://www.plosntds.org/article/info%3Adoi%2F10.1371%2Fjournal.pntd.0001305"));
		PlosScraper ps = new PlosScraper();
		assertTrue(ps.scrape(sc));
		assertTrue(ps.scrapeReferences(sc));
	
		final String reference = sc.getReferences();		
		assertNotNull(reference);
		assertTrue(reference.length() > 100);
		assertEquals("<li id=\"ref1\"><span class=\"ord".trim(), reference.substring(0, 30).trim());
		assertTrue(reference.contains("Portaels F"));
	}

}
