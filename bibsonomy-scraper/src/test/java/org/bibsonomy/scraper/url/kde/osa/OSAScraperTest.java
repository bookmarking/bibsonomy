/**
 * BibSonomy-Scraper - Web page scrapers returning BibTeX for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.scraper.url.kde.osa;

import static org.bibsonomy.scraper.junit.RemoteTestAssert.assertScraperResult;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.URL;

import org.bibsonomy.scraper.ScrapingContext;
import org.bibsonomy.scraper.UnitTestRunner;
import org.bibsonomy.scraper.junit.RemoteTest;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Scraper URL tests #93 for OSAScraper
 * @author wbi
 */
@Category(RemoteTest.class)
public class OSAScraperTest {
	
	/**
	 * starts URL test with id url_93
	 */
	@Test
	public void urlTestRun(){
		final String url = "https://www.osapublishing.org/josaa/abstract.cfm?uri=josaa-25-5-1084";
		final String resultFile = "OSAScraperUnitURLTest.bib";
		assertScraperResult(url, null, OSAScraper.class, resultFile);
	}
	
	@Ignore // because we need to login in the journal to be able to scrape the references
	@Test
	public void testReferences() throws Exception {
		final ScrapingContext sc = new ScrapingContext(new URL("https://www.osapublishing.org/josaa/abstract.cfm?uri=josaa-25-5-1084"));
		
		OSAScraper osa = new OSAScraper();
		
		assertTrue(osa.scrape(sc));
		
		assertTrue(osa.scrapeReferences(sc));
		
		final String references = sc.getReferences();
		
		assertNotNull(references);
		
		assertTrue(references.length() > 100);
		
		assertEquals("<li>\n\t\n\n\t\n\t\n\t\t\nR. K. Tyson, Principles of Adaptive Optics (Academic, 1991)".trim(), references.substring(0, 100).trim());
		
		assertTrue(references.contains("R. K. Tyson"));
	}
}
