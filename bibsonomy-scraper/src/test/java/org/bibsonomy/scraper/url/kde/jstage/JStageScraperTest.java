/**
 * BibSonomy-Scraper - Web page scrapers returning BibTeX for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.scraper.url.kde.jstage;

import static org.bibsonomy.scraper.junit.RemoteTestAssert.assertScraperResult;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.URL;

import org.bibsonomy.scraper.ScrapingContext;
import org.bibsonomy.scraper.UnitTestRunner;
import org.bibsonomy.scraper.junit.RemoteTest;
import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * @author Haile
 */
@Category(RemoteTest.class)
public class JStageScraperTest {
	/**
	 * starts URL test with id url_267
	 */
	@Test
	public void url1TestRun(){
		final String url = "https://www.jstage.jst.go.jp/article/dsj/9/0/9_CRIS4/_article";
		final String resultFile = "JStageScraperUnitURLTest.bib";
		assertScraperResult(url, null, JStageScraper.class, resultFile);
	}
	/**
	 * starts URL test with id url_276
	 */
	@Test
	public void url2TestRun(){
		final String url = "https://www.jstage.jst.go.jp/article/abas/13/2/13_67/_article";
		final String resultFile = "JStageScraperUnitURLTest2.bib";
		assertScraperResult(url, null, JStageScraper.class, resultFile);
	}
	@Test
	public void testReferences() throws Exception{
		final ScrapingContext sc = new ScrapingContext(new URL("https://www.jstage.jst.go.jp/article/dsj/9/0/9_CRIS4/_article"));
		JStageScraper js = new JStageScraper();
		assertTrue(js.scrape(sc));
		assertTrue(js.scrapeReferences(sc));
		
		final String reference = sc.getReferences();
		assertNotNull(reference);
		assertTrue(reference.length() > 100);
		assertEquals("<li>\n    \t\n    \t\n    \t\tBosnjak A. & Stempfhuber, M.(eds.) (2008) Get the Good CRIS Going: Ensuring Quality of Service for the User in the ERA. 9th International Conference on Current".trim(), reference.substring(0, 193).trim());
		assertTrue(reference.contains("Bosnjak A."));
	}
}
