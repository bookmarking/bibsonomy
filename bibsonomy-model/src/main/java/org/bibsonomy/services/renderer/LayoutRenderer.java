/**
 * BibSonomy-Model - Java- and JAXB-Model.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.services.renderer;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.bibsonomy.common.exceptions.LayoutRenderingException;
import org.bibsonomy.model.Layout;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;

/**
 * Interface for basic layout rendering. 
 * 
 * @author:  rja
 * @param <LAYOUT> - the type of layout this renderer renders
 * 
 */
public interface LayoutRenderer<LAYOUT extends Layout> {

	/** identifier for the custom layout */
	public static final String CUSTOM_LAYOUT = "custom";

	/** Returns the requested layout. A layout may be user-specific, thus the name 
	 * of the login user must be given. 
	 *  
	 * @param layoutName
	 * @param loginUserName
	 * @return The selected layout. 
	 * @throws IOException - if the layout could not be loaded because of internal errors 
	 * (i.e., not the layout itself is the problem) 
	 * @throws LayoutRenderingException - if the layout could not be found or contains errors
	 */
	public LAYOUT getLayout(final String layoutName, final String loginUserName) throws LayoutRenderingException, IOException;

	/** Renders the given layout to the outputStream.
	 * 
	 * @param layout
	 * @param posts
	 * @param embeddedLayout - if possible, the rendering result should be embeddable into
	 * a page (i.e., for HTML based layouts: don't render &lt;html&gt; tags) 
	 * @return The buffer the layout has been rendered to.
	 * 
	 * @throws IOException - if there was an internal problem rendering the layout
	 * @throws LayoutRenderingException - if the layout contains errors
	 */
	public StringBuffer renderLayout(final LAYOUT layout, final  List<? extends Post<? extends Resource>> posts, final boolean embeddedLayout) throws LayoutRenderingException, IOException;

	/** Checks, if the renderer supports the given resource type.
	 * 
	 * XXX: this could also be layout-dependent, i.e., we should not ask the
	 * renderer, but the layout ...
	 * 
	 * @param clazz
	 * @return <code>true</code>, if this renderer supports the given resource type.
	 */
	public boolean supportsResourceType(final Class<? extends Resource> clazz);
	
	/**
	 * TODO: should be a list?
	 * @return all layyouts supported by this LayoutRenderer 
	 */
	public Map<String, LAYOUT> getLayouts();
}

